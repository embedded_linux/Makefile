#include <iostream>
#include "hello.h"

using namespace std;

int main()
{
	// 新建一个CHello对象
	CHello test;
	
	// 调用其打印函数
	test.print();

	return 0;
}
